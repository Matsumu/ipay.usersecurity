﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iPay.UserSecurity.DataAccessModel.Interface
{
   public  interface iGenericRepository<TEntity>where TEntity : class
    {
      
            IQueryable<TEntity> GetAll();

            Task<TEntity> GetById(int id);

            Task<TEntity> Create(TEntity entity);

            Task Update(int id, TEntity entity);

            Task Delete(int id);
        
    }
}
