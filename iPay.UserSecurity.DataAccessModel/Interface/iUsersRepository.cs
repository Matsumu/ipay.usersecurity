﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using iPay.UserSecurity.DataAccessModel.Entities;
namespace iPay.UserSecurity.DataAccessModel.Interface
{
   public interface iUsersRepository:iGenericRepository<Users>
    {
        Task<Users> updateProfile(string customerId, Users user);
        Task<bool> blackListUser(string customerId);
        Task<bool> softDelete(string customerId);
        Task<Users> createUser(Users user);
        Task<bool> updateContactDetails(string customerId, ContactDetails contactdetails);
        Task<bool> updateContactAddress(string customerId, ContactAddress address);
        Task<bool> updateResidentialAddress(string customerId, ResidentialAddress address);
        Task<Users> getUserById(int userId);
        Task<Users> getUserByCustomerId(string customerId);
        Task<List<Users>> getUsers(List<string> customerId);
        Task<BankDetails> addBankDetails(string customerId, BankDetails model);
        Task<bool> saveChanges();

    }
}
