﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iPay.UserSecurity.DataAccessModel.Entities
{
   public class Settings:BaseEntity
    {

        public string customerType { get; set; } = "KYC1";//TODO::make sure all this values are later gotten from Config; ask operatiosn to be sure of business rules
        public string transferLimit { get; set; } = "100000";
        public bool isMobileAccess { get; set; } = true;
        public bool isWebAccess { get; set; } = true;
        public bool isUssdAccess { get; set; } = true;
        public string ussdLimit { get; set; } = "50000";
        public string mobileLimit { get; set; } = "50000";
        public string webLimit { get; set; } = "100000";
        public Users User { get; set; }
    }
}
