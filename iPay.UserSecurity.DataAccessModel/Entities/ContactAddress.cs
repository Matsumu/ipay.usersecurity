﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iPay.UserSecurity.DataAccessModel.Entities
{
   public class ContactAddress:BaseEntity
    {

        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string LandMark { get; set; }
        public string Description { get; set; }
        public virtual Users User { get; set; }
    }
}
