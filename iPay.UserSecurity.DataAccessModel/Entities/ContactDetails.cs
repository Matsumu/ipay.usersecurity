﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iPay.UserSecurity.DataAccessModel.Entities
{
    public class ContactDetails:BaseEntity
    {

        public string TelePhone { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string Fax { get; set; }
        public virtual Users User { get; set; }

    }
}
