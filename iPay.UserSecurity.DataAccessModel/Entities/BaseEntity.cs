﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using iPay.UserSecurity.DataAccessModel.Entities;


namespace iPay.UserSecurity.DataAccessModel.Entities
{
   public class BaseEntity
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int   Id { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public bool IsDelete { get; set; } = false;
        public DateTime DateModified { get; set; } = DateTime.Now;

    }
}
