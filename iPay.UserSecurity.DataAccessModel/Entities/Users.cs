﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using iPay.UserSecurity.DataAccessModel.Entities;

namespace iPay.UserSecurity.DataAccessModel.Entities
{
    public class Users:BaseEntity
    {

        [Required]
        [DataType(DataType.Text)]
        public string CustomerId { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Fname { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Lname { get; set; }
        [DataType(DataType.Text)]
        public string MiddleName { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Title { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Gender { get; set; }
        [DataType(DataType.Text)]
        public string ImagePhoto { get; set; }//convert to base64string before saving
        [Required]
        [DataType(DataType.Text)]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string EmailAddress { get; set; }
        public int ContactAddressId { get; set; }
        public virtual ContactAddress ContactAddress { get; set; }
        public int ContactDetailsId { get; set; }
        public virtual ContactDetails ContactDetails { get; set; }
        public int ResidentialAddressId { get; set; }
        public virtual ResidentialAddress ResidentialAddress { get; set;}
        public virtual ICollection<BankDetails> BankDetails { get; set; }
        public int SettingsId { get; set; }
        public virtual Settings Settings { get; set; }
        public bool isBlacklisted { get; set; } = false;



    }
}
