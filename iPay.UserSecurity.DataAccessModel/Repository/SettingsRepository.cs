﻿using iPay.UserSecurity.DataAccessModel.Entities;
using iPay.UserSecurity.DataAccessModel.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.DataAccessModel.Repository
{
   public class SettingsRepository: GenericRepository<Settings>
    {
        private UserSecurityDbContext _dbcontext;

            public SettingsRepository(UserSecurityDbContext dbContext)
            : base(dbContext)
             {
                _dbcontext = dbContext;
            }
        
    }
}
