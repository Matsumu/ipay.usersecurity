﻿using System;
using System.Collections.Generic;
using System.Text;
using iPay.UserSecurity.DataAccessModel.Interface;
using iPay.UserSecurity.DataAccessModel.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Linq;

namespace iPay.UserSecurity.DataAccessModel.Repository
{
    public class UserRepository:GenericRepository<Users>, iUsersRepository
    {

        private UserSecurityDbContext _dbcontext;
        public UserRepository(UserSecurityDbContext dbContext)
            : base(dbContext)
        {
            _dbcontext = dbContext;
        }

        public async Task<BankDetails> addBankDetails(string customerId, BankDetails model)
        {

            var item = (from c in _dbcontext.Users where c.CustomerId == customerId select c).FirstOrDefault();
            if (item != null)
            {
                model.DateModified = DateTime.Now;
                model.DateCreated = DateTime.Now;
                item.BankDetails.Add(model);
                _dbcontext.Update(item);
                var result = await saveChanges();

            }
            return model;
        }

        public async Task<bool> blackListUser(string customerId)
        {
            var result = false;
            var item = (from c in _dbcontext.Users where c.CustomerId == customerId select c).FirstOrDefault();
            if (item != null)
            {
                item.isBlacklisted = true;
                item.DateModified = DateTime.Now;
                _dbcontext.Update(item);
                result = await saveChanges();
            }

            return result;
        }

        public async Task<Users> createUser(Users user)
        {
            ContactAddress cxtAdd = new ContactAddress();
            ContactDetails ctxc= new ContactDetails();
            ResidentialAddress resadd = new ResidentialAddress();
            Settings sett = new Settings();
            Users result = null;
            user.DateCreated = DateTime.Now;
            user.DateModified = DateTime.Now;
            _dbcontext.ContactAddress.Add(cxtAdd);
            _dbcontext.ContactDetails.Add(ctxc);
            _dbcontext.ResidentialAddress.Add(resadd);
            _dbcontext.Settings.Add(sett);

            //try and refactor this secttion; it my fail in the near future
            await saveChanges();
            user.SettingsId = sett.Id;
            user.ContactDetailsId = ctxc.Id;
            user.ContactAddressId = cxtAdd.Id;
            user.ResidentialAddressId = resadd.Id;
            await _dbcontext.Users.AddAsync(user);
            await saveChanges();
            result = user;
            return result;
            //TODO::make sure contact details, contact address, residential details and settings is created 
            
        }

        public async Task<Users> getUserByCustomerId(string customerId)
        {
            Users customer = new Users();
            var result = (from c in _dbcontext.Users where c.CustomerId == customerId select c).FirstOrDefault();
            customer = result;
            return customer;
        }

        public async Task<Users> getUserById(int userId)
        {
            Users customer = null;
            Users result = (from c in _dbcontext.Users where c.Id == userId select c).FirstOrDefault();
            customer = result;
            return customer;
        }

        public async Task<List<Users>> getUsers(List<string> customerId)
        {
            List<Users> usersList = null;

            foreach (var item in customerId)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    var items = (from c in _dbcontext.Users where c.CustomerId == item select c).FirstOrDefault();
                    usersList.Add(items);
                }
            }
            return usersList;
        }

        public async Task<bool> saveChanges()
        {
            var result = false;
            var save= await _dbcontext.SaveChangesAsync();
            if (save>0)
            {
                result = true;
            }

            return result;
        }

        public async Task<bool> softDelete(string customerId)
        {
            var result = false;
            var item = (from c in _dbcontext.Users where c.CustomerId == customerId select c).FirstOrDefault();
            if (item!=null)
            {
                item.IsDelete = true;
                item.DateModified = DateTime.Now;
                _dbcontext.Update(item);
               result=await  saveChanges();

            }

            return result;
        }

        public async Task<bool> updateContactAddress(string customerId, ContactAddress address)
        {
            var result = false;
            var item = (from c in _dbcontext.Users where c.CustomerId == customerId select c).FirstOrDefault();
            if (item != null)
            {
                item.ContactAddress.City = string.IsNullOrEmpty(address.City) || string.IsNullOrWhiteSpace(address.City) ? item.ContactAddress.City : address.City;
                item.ContactAddress.Country = string.IsNullOrEmpty(address.Country) || string.IsNullOrWhiteSpace(address.Country) ? item.ContactAddress.Country : address.Country;
                item.ContactAddress.DateModified = DateTime.Today;
                item.ContactAddress.Description = string.IsNullOrEmpty(address.Description) || string.IsNullOrWhiteSpace(address.Description) ? item.ContactAddress.Description : address.Description;
                item.ContactAddress.HouseNumber = string.IsNullOrEmpty(address.HouseNumber) || string.IsNullOrWhiteSpace(address.HouseNumber) ? item.ContactAddress.HouseNumber : address.HouseNumber;
                item.ContactAddress.LandMark = string.IsNullOrEmpty(address.LandMark) || string.IsNullOrWhiteSpace(address.LandMark) ? item.ContactAddress.LandMark : address.LandMark;
                item.ContactAddress.State = string.IsNullOrEmpty(address.State) || string.IsNullOrWhiteSpace(address.State) ? item.ContactAddress.State : address.State;
                item.ContactAddress.Street = string.IsNullOrEmpty(address.Street) || string.IsNullOrWhiteSpace(address.Street) ? item.ContactAddress.Street : address.Street;

                _dbcontext.Update(item);
                result = await saveChanges();

            }
            return result;
        }

        public async Task<bool> updateContactDetails(string customerId, ContactDetails contactdetails)
        {
            var result = false;
            var item = (from c in _dbcontext.Users where c.CustomerId == customerId select c).FirstOrDefault();
            if (item != null)
            {
                item.ContactDetails.EmailAddress = string.IsNullOrEmpty(contactdetails.EmailAddress) || string.IsNullOrWhiteSpace(contactdetails.EmailAddress) ? item.ContactDetails.EmailAddress : contactdetails.EmailAddress;
                item.ContactDetails.TelePhone = string.IsNullOrEmpty(contactdetails.TelePhone) || string.IsNullOrWhiteSpace(contactdetails.TelePhone) ? item.ContactDetails.TelePhone : contactdetails.TelePhone;
                item.ContactDetails.DateModified = DateTime.Today;
                item.ContactDetails.Mobile = string.IsNullOrEmpty(contactdetails.Mobile) || string.IsNullOrWhiteSpace(contactdetails.Mobile) ? item.ContactDetails.Mobile : contactdetails.Mobile;
                item.ContactDetails.Fax = string.IsNullOrEmpty(contactdetails.Fax) || string.IsNullOrWhiteSpace(contactdetails.Fax) ? item.ContactDetails.Fax : contactdetails.Fax;
                item.EmailAddress = string.IsNullOrEmpty(contactdetails.EmailAddress) || string.IsNullOrWhiteSpace(contactdetails.EmailAddress) ? item.ContactDetails.EmailAddress : contactdetails.EmailAddress;

                _dbcontext.Update(item);
                result = await saveChanges();

            }
            return result;
        }

        public async Task<Users> updateProfile(string customerId, Users user)
        {
            Users result = null;
            var item = (from c in _dbcontext.Users where c.CustomerId == customerId select c).FirstOrDefault();
            if (item != null)
            {
                item.EmailAddress = string.IsNullOrEmpty(user.EmailAddress) || string.IsNullOrWhiteSpace(user.EmailAddress) ? item.EmailAddress : user.EmailAddress;//do a check and confirm email exist and userservice end
                item.Fname = string.IsNullOrEmpty(user.Fname) || string.IsNullOrWhiteSpace(user.Fname) ? item.Fname : user.Fname;
                item.Title = string.IsNullOrEmpty(user.Title) || string.IsNullOrWhiteSpace(user.Title) ? item.Title : user.Title;
                item.Lname = string.IsNullOrEmpty(user.Lname) || string.IsNullOrWhiteSpace(user.Lname) ? item.Lname : user.Lname;
                item.UserName = string.IsNullOrEmpty(user.UserName) || string.IsNullOrWhiteSpace(user.UserName) ? item.UserName : user.UserName;//do a check and confirm if username exist at user service end
                item.Gender = string.IsNullOrEmpty(user.Gender) || string.IsNullOrWhiteSpace(user.Gender) ? item.Gender : user.Gender;
                item.DateModified = DateTime.Now;

                //TODO: Implement duplicate check for username and email address;
                //TODO: implement update for contact details too for email address, username in vice-versa other;
                
                _dbcontext.Update(item);
                var res = await saveChanges();
                result = item;

            }
            return result;
        }

        public async Task<bool> updateResidentialAddress(string customerId, ResidentialAddress address)
        {
            var result = false;
            var item = (from c in _dbcontext.Users where c.CustomerId == customerId select c).FirstOrDefault();
            if (item != null)
            {
                item.ResidentialAddress.City = string.IsNullOrEmpty(address.City) || string.IsNullOrWhiteSpace(address.City) ? item.ContactAddress.City : address.City;
                item.ResidentialAddress.Country = string.IsNullOrEmpty(address.Country) || string.IsNullOrWhiteSpace(address.Country) ? item.ContactAddress.Country : address.Country;
                item.ResidentialAddress.DateModified = DateTime.Today;
                item.ResidentialAddress.Description = string.IsNullOrEmpty(address.Description) || string.IsNullOrWhiteSpace(address.Description) ? item.ContactAddress.Description : address.Description;
                item.ResidentialAddress.HouseNumber = string.IsNullOrEmpty(address.HouseNumber) || string.IsNullOrWhiteSpace(address.HouseNumber) ? item.ContactAddress.HouseNumber : address.HouseNumber;
                item.ResidentialAddress.LandMark = string.IsNullOrEmpty(address.LandMark) || string.IsNullOrWhiteSpace(address.LandMark) ? item.ContactAddress.LandMark : address.LandMark;
                item.ResidentialAddress.State = string.IsNullOrEmpty(address.State) || string.IsNullOrWhiteSpace(address.State) ? item.ContactAddress.State : address.State;
                item.ResidentialAddress.Street = string.IsNullOrEmpty(address.Street) || string.IsNullOrWhiteSpace(address.Street) ? item.ContactAddress.Street : address.Street;

                _dbcontext.Update(item);
                result = await saveChanges();

            }
            return result;
        }
    }
}
