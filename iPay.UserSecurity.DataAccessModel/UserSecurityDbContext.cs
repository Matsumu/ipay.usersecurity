﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using iPay.UserSecurity.DataAccessModel.Entities;

namespace iPay.UserSecurity.DataAccessModel
{
  public  class UserSecurityDbContext: DbContext
    {
        public UserSecurityDbContext(DbContextOptions<UserSecurityDbContext> options)
            :base(options)
        {
            base.Database.EnsureCreated();

        }


        public DbSet<BankDetails> BankDetails { get; set; }
        public DbSet<ContactAddress> ContactAddress { get; set; }
        public DbSet<ContactDetails> ContactDetails { get; set; }
        public DbSet<ResidentialAddress> ResidentialAddress { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<Settings> Settings { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
        }
    }
}
