﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iPay.UserSecurity.Api.Filters;
using iPay.UserSecurity.DataAccessModel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using iPay.UserSecurity.DataAccessModel.Repository;
using iPay.UserSecurity.Core.Interface;
using iPay.UserSecurity.Core.Services;
using iPay.UserSecurity.DataAccessModel.Interface;


namespace iPay.UserSecurity.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var value = Configuration["DbConnectionString:TestDBLocal"];
            services.AddTransient<iUsersRepository, UserRepository>();
            services.AddTransient<iUserService, UserService>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddScoped<ModelValidationAttribute>();
            services.AddDbContext<UserSecurityDbContext>(options =>
           options.UseSqlServer(Configuration["DbConnectionString:TestDBRemote"]));
           

            // Register the Swagger generator
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("IST.iPay.Usersecurity", new Info
                {
                    Version = "V1.0.0.0",
                    Title = "IST.iPay.Usersecurity",
                    Description = "IST.iPay.Usersecurity MicroService",
                    TermsOfService = "All Right Reserved IST 2019",
                    Contact = new Contact
                    {
                        Name = "IST.iPay.Usersecurity",
                        Email = "IST@Infostrategytech.com",
                        Url = "dev.IST.Usersecurity.Infostrategy.com"
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/IST.iPay.Usersecurity/swagger.json", "IST.iPay.Usersecurity");
            });


            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
