﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using iPay.UserSecurity.Core.Interface;
using iPay.UserSecurity.Core.Requests;
using iPay.UserSecurity.Core.Responses;


namespace iPay.UserSecurity.Api.Controllers
{
    [Route("api/UserManaGement")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly iUserService _service;
        public UserController(ILogger<UserController> logger, iUserService service)
        {
            _logger = logger;
            _service = service;
        }

        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <returns>The created user.</returns>
        /// <param name="model">Model.</param>
        [HttpPost]
        [Route("CreateUser")]
        public async Task<IActionResult> CreateUser([FromBody]CreateUserequest model)
        {
            var res = new CreateuserResponse();
            try
            {
                if (model==null)
                {
                    return BadRequest("model is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.createUser(model);
            }
            catch (Exception ex)
            {
                res.response.Description = ex.Message;
                _logger.LogError("An Error has occured"+ ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }

        /// <summary>
        /// Get list of users.
        /// </summary>
        /// <returns>return list of users.</returns>
        /// <param name="customerId">list of Customer identifier.</param>
        [HttpPost]
        [Route("GetUsers")]
        public async Task<IActionResult> GetUsers([FromBody]List<string> customerId)
        {
            var res = new GetUsersResponse();
            try
            {
                if (customerId == null)
                {
                    return BadRequest("object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.getUsers(customerId);
            }
            catch (Exception ex)
            {
                res.response.Description = ex.Message;
                _logger.LogError("An Error has occured" + ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }

        /// <summary>
        /// Get a single user.
        /// </summary>
        /// <returns>user</returns>
        /// <param name="customerId">Customer identifier.</param>
        [HttpGet]
        [Route("GetUserBycustomerId/{customerId}")]
        public async Task<IActionResult> GetUser(string customerId)
        {
            var res = new GetUserResponse();
            try
            {
                if (customerId == null)
                {
                    return BadRequest("object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.getUserByCustomerId(customerId);
            }
            catch (Exception ex)
            {
                res.response.Description = ex.Message;
                _logger.LogError("An Error has occured" + ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }

        /// <summary>
        /// Blacklist the user.
        /// </summary>
        /// <returns>Base response.</returns>
        /// <param name="customerId">Customer identifier.</param>
        [HttpGet]
        [Route("BlackListUser/{customerId}")]
        public async Task<IActionResult> BlackListUser(string customerId)
        {
            var res = new BaseResponse();
            try
            {
                if (customerId == null)
                {
                    return BadRequest("object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.blackListUser(customerId);
            }
            catch (Exception ex)
            {
                res.Description = ex.Message;
                _logger.LogError("An Error has occured" + ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }

        /// <summary>
        /// flag an entity for delete;
        /// </summary>
        /// <returns>The delete.</returns>
        /// <param name="customerId">Customer identifier.</param>
        [HttpGet]
        [Route("SoftDelete/{customerId}")]
        public async Task<IActionResult> SoftDelete(string customerId)
        {
            var res = new BaseResponse();
            try
            {
                if (customerId == null)
                {
                    return BadRequest("object is null");
                    
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.softDelete(customerId);
            }
            catch (Exception ex)
            {
                res.Description = ex.Message;
                _logger.LogError("An Error has occured" + ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }

        /// <summary>
        /// Addbankdetails the specified customerId and model.
        /// </summary>
        /// <returns>The addbankdetails.</returns>
        /// <param name="customerId">Customer identifier.</param>
        /// <param name="AddBankDetailsRequest">Model.</param>

        [HttpPost]
        [Route("Addbankdetails/{customerId}")]
        public async Task<IActionResult> Addbankdetails(string customerId, AddBankDetailsRequest model)
        {
            var res = new AddBankDetailsResponse();
            try
            {
                if (customerId == null)
                {
                    return BadRequest("object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.addBankDetails(customerId, model);
            }
            catch (Exception ex)
            {
                res.response.Description = ex.Message;
                _logger.LogError("An Error has occured" + ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }


        /// <summary>
        /// Update residentials the address.
        /// </summary>
        /// <returns>customer id of the updated residential address.</returns>
        /// <param name="customerId">Customer identifier.</param>
        /// <param name="updateResidentialAddressRequest">residential address model.</param>
        [HttpPost]
        [Route("UpdateresidentialAddress/{customerId}")]
        public async Task<IActionResult> UpdateresidentialAddress(string customerId, updateResidentialAddressRequest model)
        {
            var res = new UpdateResidentialAddressResponse();
            try
            {
                if (customerId == null)
                {
                    return BadRequest("object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.updateresidentialAddress(customerId, model);
            }
            catch (Exception ex)
            {
                res.response.Description = ex.Message;
                _logger.LogError("An Error has occured" + ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }

        /// <summary>
        /// Updates the contact address.
        /// </summary>
        /// <returns>the custmerId of the updated  contact address.</returns>
        /// <param name="customerId">Customer identifier.</param>
        /// <param name="UpdateContactAddressRequest">contact address model.</param>
        [HttpPost]
        [Route("updateContactAddress/{customerId}")]
        public async Task<IActionResult> updateContactAddress(string customerId, UpdateContactAddressRequest model)
        {
            var res = new UpdateContactAddressResponse();
            try
            {
                if (customerId == null)
                {
                    return BadRequest("object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.updateContactAddress(customerId, model);
            }
            catch (Exception ex)
            {
                res.response.Description = ex.Message;
                _logger.LogError("An Error has occured" + ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }


        /// <summary>
        /// Updates the contact details.
        /// </summary>
        /// <returns>the customerId of the updated  contact details.</returns>
        /// <param name="customerId">Customer identifier.</param>
        /// <param name="model">contact details model.</param>
        [HttpPost]
        [Route("UpdateContactDetails/{customerId}")]
        public async Task<IActionResult> UpdateContactDetails(string customerId, UpdateContactDetailsRequest model)
        {
            var res = new UpdateContactDetailsResponse();
            try
            {
                if (customerId == null)
                {
                    return BadRequest("object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.updateContactDetails(customerId, model);
            }
            catch (Exception ex)
            {
                res.response.Description = ex.Message;
                _logger.LogError("An Error has occured" + ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }

        /// <summary>
        /// Updates the profile.
        /// </summary>
        /// <returns>The customerId of the updated profile.</returns>
        /// <param name="customerId">Customer identifier.</param>
        /// <param name="model">Model.</param>
        [HttpPost]
        [Route("UpdateProfile/{customerId}")]
        public async Task<IActionResult> UpdateProfile(string customerId, UpdateProfileRequest model)
        {
            var res = new updateProfileResponse();
            try
            {
                if (customerId == null)
                {
                    return BadRequest("object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                res = await _service.updateProfile(customerId, model);
            }
            catch (Exception ex)
            {
                res.response.Description = ex.Message;
                _logger.LogError("An Error has occured" + ex.Message);
                StatusCode(500, ex.Message);
            }

            return Ok(res);
        }


    }
}