﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iPay.UserSecurity.Core.Requests
{
   public class AddBankDetailsRequest
    {
        [Required]
        [DataType(DataType.Text)]
        public string AccountNumber { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string AccountName { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string BankName { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string BankCode { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string SortCode { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string BankType { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string SwiftCode { get; set; }
    }
}
