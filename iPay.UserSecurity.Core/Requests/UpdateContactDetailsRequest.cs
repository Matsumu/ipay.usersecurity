﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Requests
{
    public class UpdateContactDetailsRequest
    {
        public string TelePhone { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string Fax { get; set; }
    }
}
