﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Requests
{
   public class UpdateProfileRequest
    {
     
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string MiddleName { get; set; }
        public string Title { get; set; }
        public string Gender { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }

    }
}
