﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Requests
{
    public class UpdateContactAddressRequest
    {
        public string State { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string LandMark { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
    }
}
