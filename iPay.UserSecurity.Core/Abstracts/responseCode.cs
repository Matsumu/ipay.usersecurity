﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Abstracts
{
    public static class responseCode
    {
        public static string error { get; } = "01";
        public static string success { get; } = "00";
        public static string pending { get; } = "02";

    }
}
