﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Abstracts
{
    public static class responseDescription
    {
        public static string success { get; } = "operation completely successfully";
        public static string error { get; } = "an error has occured, operation failed";
        public static string pending { get; } = "request/operation is still pending";
    }
}
