﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Abstracts
{
   public static  class responseMessages
    {
        public static string success { get; } = "Success/Completed";
        public static string error { get; } = "Error/Not Completed";
        public static string Pending { get; } = "Pending/suspending";
    }
}
