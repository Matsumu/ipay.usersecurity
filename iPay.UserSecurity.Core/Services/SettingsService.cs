﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using iPay.UserSecurity.Core.Interface;
namespace iPay.UserSecurity.Core.Services
{
    public class SettingsService : iSettingsService
    {
        public Task<bool> mobileAccess(string customerId, int amount)
        {
            throw new NotImplementedException();
        }

        public Task<string> updateMobileLimit(string customerId, int amount)
        {
            throw new NotImplementedException();
        }

        public Task<string> updateTransactionLimit(string customerId, int amount)
        {
            throw new NotImplementedException();
        }

        public Task<string> updateUssdLimit(string customerId, int amount)
        {
            throw new NotImplementedException();
        }

        public Task<string> updateWebLimit(string customerId, int amount)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ussdAccess(string customerId, int amount)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ussdWebAccess(string customerId, int amount)
        {
            throw new NotImplementedException();
        }
    }
}
