﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using iPay.UserSecurity.Core.Interface;
using iPay.UserSecurity.Core.Requests;
using iPay.UserSecurity.Core.Responses;
using iPay.UserSecurity.DataAccessModel.Repository;
using iPay.UserSecurity.DataAccessModel.Interface;
using iPay.UserSecurity.DataAccessModel.Entities;
using iPay.UserSecurity.Core.Abstracts;

namespace iPay.UserSecurity.Core.Services
{
    public class UserService : iUserService
    {
        //refactor project later to use automapper
        private iUsersRepository _repo;
        public UserService(iUsersRepository repo)
        {
            _repo = repo;
        }

        /// <summary>
        /// Adding New bank details to a user;
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<AddBankDetailsResponse> addBankDetails(string customerId, AddBankDetailsRequest request)
        {
            AddBankDetailsResponse res = new AddBankDetailsResponse();
            if (request!=null)
            {
                Users userId = await _repo.getUserByCustomerId(customerId);

                var user = _repo.addBankDetails(customerId, new BankDetails
                {
                    AccountName = request.AccountName,
                    AccountNumber = request.AccountNumber,
                    BankCode = request.BankCode,
                    BankName = request.BankName,
                    BankType = request.BankType,
                    SortCode = request.SortCode,
                    SwiftCode = request.SwiftCode,
                    UserId = userId.Id
                   
                });

                if (user!=null)
                {
                    res.response.Description = responseDescription.success;
                    res.response.statusMessage = responseMessages.success;
                    res.response.statusCode = responseCode.success;
                }
            }


            return res;
            
        }

        /// <summary>
        /// Blacklisting user for fraud or serice misuse;
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public async Task<BaseResponse> blackListUser(string customerId)
        {
            BaseResponse res = new BaseResponse();
                var result = await _repo.blackListUser(customerId);
            if (result)
            {
                res.Description = responseDescription.success;
                res.statusMessage = responseMessages.success;
                res.statusCode = responseCode.success;
            }
            return res;
        }

        /// <summary>
        ///  register new user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<CreateuserResponse> createUser(CreateUserequest request)
        {
            CreateuserResponse res = new CreateuserResponse();
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var generatedCustomerId = unixTimestamp.ToString();
            if (request!=null)
            {
                var result = await _repo.createUser(
                    new Users
                    {
                        UserName = request.UserName,
                        EmailAddress = request.EmailAddress,
                        CustomerId = generatedCustomerId,
                        Fname=request.Fname,
                        Lname=request.Lname,
                        MiddleName=request.MiddleName,
                        Gender=request.Gender,
                        Title=request.Title,
                    });

                if (result!=null)
                {
                    res.response.Description = responseDescription.success;
                    res.response.statusMessage = responseMessages.success;
                    res.response.statusCode = responseCode.success;
                    res.customerId = generatedCustomerId;
                    res.userId = result.Id;

                }

            }
            return res;
        }

        /// <summary>
        /// get registered user by customerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public async Task<GetUserResponse> getUserByCustomerId(string customerId)
        {
            GetUserResponse res = new GetUserResponse();
            if (!string.IsNullOrEmpty(customerId) || !string.IsNullOrWhiteSpace(customerId))
            {
                var result = await  _repo.getUserByCustomerId(customerId);
                if (result!=null)
                {
                    res.response.Description = responseDescription.success;
                    res.response.statusMessage = responseMessages.success;
                    res.response.statusCode = responseCode.success;
                    res.customerId = result.CustomerId;
                    res.userId = result.Id;

                }
            }
            return res;
        }

        /// <summary>
        /// Get registered user by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<GetUserResponse> getUserById(int  userId)
        {
            GetUserResponse res = new GetUserResponse();
            if (!string.IsNullOrEmpty(userId.ToString()) || !string.IsNullOrWhiteSpace(userId.ToString()))
            {
                var result = await _repo.getUserById(userId);
                if (result != null)
                {
                    res.response.Description = responseDescription.success;
                    res.response.statusMessage = responseMessages.success;
                    res.response.statusCode = responseCode.success;
                    res.customerId = result.CustomerId;
                    res.userId = result.Id;

                }
            }
            return res;
        }

        /// <summary>
        /// Get list of users by looping through list of customerid's
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public async Task<GetUsersResponse> getUsers(List<string> customerId)
        {
            GetUsersResponse res = new GetUsersResponse();
            if (customerId!=null)
            {
                var result = await _repo.getUsers(customerId);
                if (result != null)
                {
                    res.response.Description = responseDescription.success;
                    res.response.statusMessage = responseMessages.success;
                    res.response.statusCode = responseCode.success;
                    res.users = result;

                }
            }
            return res;
        }

     
        /// <summary>
        /// templorarily delete user before it can be archieve at a particular period of time;
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>

        public async Task<BaseResponse> softDelete(string customerId)
        {
            BaseResponse res = new BaseResponse();
            if (string.IsNullOrEmpty(customerId) || string.IsNullOrWhiteSpace(customerId))
            {
                var result = await _repo.softDelete(customerId);
                if (result)
                {
                    res.Description = responseDescription.success;
                    res.statusMessage = responseMessages.success;
                    res.statusCode = responseCode.success;

                }

            }
            return res;

        }
       

        /// <summary>
        /// update user residential address based on the supplied customerId;s
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<UpdateResidentialAddressResponse> updateresidentialAddress(string customerId, updateResidentialAddressRequest request)
        {
            UpdateResidentialAddressResponse res = new UpdateResidentialAddressResponse();
            if (!string.IsNullOrEmpty(customerId) && !string.IsNullOrWhiteSpace(customerId) && request !=null)
            {

                var result = await _repo.updateResidentialAddress(customerId, 
                new ResidentialAddress {
                    City=request.City,
                    Country=request.Country,
                    State=request.State,
                    Street=request.Street,
                    HouseNumber=request.HouseNumber,
                    Description=request.Description,
                    LandMark=request.LandMark,
                    DateModified=DateTime.Now
                });
                 
                if (result)
                {
                    res.response.Description = responseDescription.success;
                    res.response.statusMessage = responseMessages.success;
                    res.response.statusCode = responseCode.success;
                    res.customerId = customerId;
                }
            }

            return res;

        }

        /// <summary>
        /// update customer contact address based on  supplied customerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="request"></param>
        /// <returns></returns>

        public async Task<UpdateContactAddressResponse> updateContactAddress(string customerId, UpdateContactAddressRequest request)
        {
            UpdateContactAddressResponse res = new UpdateContactAddressResponse();

            if (!string.IsNullOrEmpty(customerId) && request!=null)
            {
                var result = await _repo.updateContactAddress(customerId, new ContactAddress {
                    City = request.City,
                    Country = request.Country,
                    State = request.State,
                    Street = request.Street,
                    HouseNumber = request.HouseNumber,
                    Description = request.Description,
                    LandMark = request.LandMark,
                    DateModified = DateTime.Now

                });

                if (result)
                {
                    res.response.Description = responseDescription.success;
                    res.response.statusMessage = responseMessages.success;
                    res.response.statusCode = responseCode.success;
                    res.customerId = customerId;
                }

            }

            return res;
        }
        /// <summary>
        /// update customer contact details based on supplied address
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<UpdateContactDetailsResponse> updateContactDetails(string customerId, UpdateContactDetailsRequest request)
        {
            UpdateContactDetailsResponse res = new UpdateContactDetailsResponse();

            if (!string.IsNullOrEmpty(customerId) && request != null)
            {
                var result = await _repo.updateContactDetails(customerId, new ContactDetails { 
                EmailAddress=request.EmailAddress,
                TelePhone=request.TelePhone,
                Mobile=request.Mobile,
                Fax=request.Fax,
                DateModified=DateTime.Now
                
                });

                if (result)
                {
                    res.response.Description = responseDescription.success;
                    res.response.statusMessage = responseMessages.success;
                    res.response.statusCode = responseCode.success;
                    res.customerId = customerId;
                }
            }

            return res;
        }
        /// <summary>
        /// update basic user profile data
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<updateProfileResponse> updateProfile(string customerId, UpdateProfileRequest request)
        {
            updateProfileResponse res = new updateProfileResponse();
            if (!string.IsNullOrEmpty(customerId) && request != null) {

                var result = await _repo.updateProfile(customerId, new Users {
                    //TODO: ask for clarification who change of name, email and username;
                    //TODO: email adress and username changes should be run against duplicates records=> this should be done and registration and change of credentials
                  });
               }
            return res;
        }
    }
}
