﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using iPay.UserSecurity.Core.Requests;
using iPay.UserSecurity.Core.Responses;


namespace iPay.UserSecurity.Core.Interface
{
   public interface iUserService
    {
        Task<CreateuserResponse> createUser(CreateUserequest request);
        Task<BaseResponse> blackListUser(string customerId);
        Task<BaseResponse> softDelete(string customerId);
        Task<updateProfileResponse> updateProfile(string customerId, UpdateProfileRequest request);
        Task<UpdateContactDetailsResponse> updateContactDetails(string customerId, UpdateContactDetailsRequest request);
        Task<UpdateContactAddressResponse> updateContactAddress(string customerId, UpdateContactAddressRequest request);
        Task<UpdateResidentialAddressResponse> updateresidentialAddress(string customerId, updateResidentialAddressRequest request);
        Task<AddBankDetailsResponse> addBankDetails(string customerId, AddBankDetailsRequest request);
        Task<GetUserResponse> getUserById(int userId);
        Task<GetUserResponse> getUserByCustomerId(string customerId);
        Task<GetUsersResponse> getUsers(List<string> customerId);

    }
}
