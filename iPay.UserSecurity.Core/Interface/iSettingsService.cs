﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iPay.UserSecurity.Core.Interface
{
   public interface iSettingsService
    {
        Task<string> updateTransactionLimit(string customerId, int amount);
        Task<string> updateUssdLimit(string customerId, int amount);
        Task<string> updateWebLimit(string customerId, int amount);
        Task<string> updateMobileLimit(string customerId, int amount);
        Task<bool> ussdAccess(string customerId, int amount);
        Task<bool> ussdWebAccess(string customerId, int amount);
        Task<bool> mobileAccess(string customerId, int amount);
    }
}
