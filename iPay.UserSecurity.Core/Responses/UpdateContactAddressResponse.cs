﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Responses
{
   public class UpdateContactAddressResponse
    {
        public BaseResponse response { get; set; } = new BaseResponse();
        public string customerId { get; set; }
    }
}
