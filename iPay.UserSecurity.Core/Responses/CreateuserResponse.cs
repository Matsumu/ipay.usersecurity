﻿using System;
namespace iPay.UserSecurity.Core.Responses
{
    public class CreateuserResponse
    {
        public string customerId { get; set; }
        public int userId { get; set; }
        public BaseResponse response { get; set; } = new BaseResponse();

    }
}
