﻿using iPay.UserSecurity.DataAccessModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Responses
{
  public  class GetUsersResponse
    {
        public BaseResponse response { get; set; } = new BaseResponse();
        public List<Users> users { get; set; } = new List<Users>();
    }
}
