﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Responses
{
   public class updateProfileResponse
    {
        public BaseResponse response { get; set; } = new BaseResponse();
        public string customerId { get; set; }
    }
}
