﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Responses
{
    public class BaseResponse
    {
        public string statusCode { get; set; } = "01";
        public string statusMessage { get; set; } = "Error/Not Completed";
        public string Description { get; set; } = "request/operation is still pending";
    }
}
