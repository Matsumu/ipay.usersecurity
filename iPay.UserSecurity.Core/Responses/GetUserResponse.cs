﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iPay.UserSecurity.Core.Responses
{
  public  class GetUserResponse
    {
        public string customerId { get; set; }
        public int userId { get; set; }
        public BaseResponse response { get; set; } = new BaseResponse();

    }
}
